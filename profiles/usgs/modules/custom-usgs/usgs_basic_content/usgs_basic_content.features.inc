<?php
/**
 * @file
 * usgs_basic_content.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function usgs_basic_content_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "panelizer" && $api == "panelizer") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function usgs_basic_content_node_info() {
  $items = array(
    'usgs_basic_content' => array(
      'name' => t('USGS Basic Content'),
      'base' => 'node_content',
      'description' => t('Use <em>USGS Content</em> to add any <em>piece</em> of individual content to be used on the USGS website.  This content type serves as the primary building block for all content on the USGS website.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
