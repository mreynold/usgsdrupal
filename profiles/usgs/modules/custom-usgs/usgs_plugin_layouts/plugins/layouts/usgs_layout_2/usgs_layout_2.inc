<?php

$plugin = array(
    'title' => t('USGS Layout Two'),
    'category' => t('USGS Layouts'),
    'icon' => 'usgs_layout_2.png',
    'theme' => 'usgs_layout_2',
    'css' => 'usgs_layout_2.css',
    'admin css' => 'usgs_layout_2_admin.css',
    'regions' => array(
      'carousel' => t('Carousel region'),
      'main' => t('Main region'),
      'left' => t('Left region'),
    ),
  );

/**
 * Implementation of hook_preprocess().
 *
 * Mainly to add the required CSS for proper layout rendering while in the admin
 * pages.
 *
 * @todo  Need to create some custom css so that this will render properly in admin
 *        pages. Specifically, we will need to add some code to add the proper wrappers
 *        required by bootstrap to make the columns and floats work i.e. container and row
 *        classes.
 *
 * @param type $vars
 */
function usgs_plugin_layouts_preprocess_usgs_layout_2(&$vars) {
  global $theme;

  $themes = list_themes();

  if(isset($themes['palladium_bootstrap'])) {
    if ($themes['palladium_bootstrap']->status) {
      $options = array(
        'group' => CSS_THEME,
        'weight' => 100,
      );
      // Commented this out because this will override a lot of the seven admin theme
      // styles and make the admin page look really funny.
      // @ todo need to add some custom styles and sheet that we can add here instead
      // of the style.css one from palladium_bootstrap.
      //drupal_add_css(drupal_get_path('theme', 'palladium_bootstrap') . '/css/style.css', $options);
    }
  }
}