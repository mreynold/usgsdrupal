<?php
  //dsm(get_defined_vars());
?>

<div class="row panel-display panel-usgs-layout-1 clearfix" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>
  <div class="col-md-2 panel-panel panel-region-left">
    <div class="inside"><?php print $content['left']; ?></div>
  </div>

  <div class="col-md-10">
    <div class="row">
      <div class="col-md-12 panel-panel panel-region-top">
        <div class="inside"><?php print $content['top']; ?></div>
      </div>    
    </div>
    <div class="row">
      <div class="col-md-12 panel-panel panel-region-main">
        <div class="inside"><?php print $content['main']; ?></div>
      </div>
    </div>
  </div>
  
</div>
