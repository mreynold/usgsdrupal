<?php
/**
 * @file
 * usgs_basic_content.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function usgs_basic_content_taxonomy_default_vocabularies() {
  return array();
}
