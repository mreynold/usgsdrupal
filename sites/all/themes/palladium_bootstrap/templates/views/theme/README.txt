This folder is where I put all the views template files that I want to override.
First I copy and paste all the template files from the views module here, then move
them into the parent directory where I rename it to do the override.

For the most part, this folder should always be empty.