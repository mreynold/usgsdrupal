style.less is the main less file. It calls all the needed bootstrap less files.

It is important to note that this file will call the bootstrap.less file 
located in this directory.  This bootstrap.less file will call the bootstrap
files and all overrideden ones will be modified here.